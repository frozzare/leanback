include $(GOPATH)/src/Make.inc

TARG=couch
GOFMT=gofmt -spaces=true -tabindent=false -tabwidth=4

GOFILES=\
	couch.go\

include $(GOPATH)/src/Make.pkg

format:
	${GOFMT} -w couch.go
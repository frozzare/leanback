package couch

import (
  "io"
  "io/ioutil"
  "net/http"
  "encoding/json"
  //"fmt"
  "strconv"
  "bytes"
)

type CouchError struct {
  Op string
  Message string
}

func (e CouchError) String() string { return e.Op + ":" + e.Message }

type Server struct {
  host string
}

type Request struct {
  url string
}

type Leanback struct {
  host string
  database string
  req Request
}

type Row struct {
  id    string
  key   string
  value string
}

type Results struct {
  total_rows int
  offset     int
  rows       []Row
}

type Document map[string]interface{}

func ReadResponse(body io.Reader) (string, string) {
  data, err := ioutil.ReadAll(body)
  
  if err != nil {
    return "", "Error ReadResponse"//, &CouchError{"couch.ReadResponse", "Something didnt work"}
  }
  
  return string(data), ""//, nil
}

func toDocument(body io.Reader) (Document, string) {
  bytes, err := ioutil.ReadAll(body)
  
  var doc Document
  
  json.Unmarshal(bytes, &doc)
  
  if err != nil {
    return nil, "Error converting body to document"
  }
  
  return doc, ""
}

func toResults(body io.Reader) (Results) {
  bytes, _ := ioutil.ReadAll(body)
  
  var results Results
  
  json.Unmarshal(bytes, &results)
  
  return results
}

func Couch(host string) (Server) {
  return Server{host}
}

func (s *Server) use(database string) (Leanback) {
  return Leanback{s.host, database, Request{s.host + "/" + database}}
}

func (lb *Leanback) get(docid string) (Document, string) {
  res, err := lb.req.do("GET", docid, "")
  
  if len(err) > 0 {
    return nil, "Request error"
  }
  
  return toDocument(res.Body)
}

func (lb *Leanback) save(data string) (Document, string) {
  res, err := lb.req.do("POST", "/", data)

  if len(err) > 0 {
    return nil, "Request error"
  }
  
  return toDocument(res.Body)
}

func (lb *Leanback) exists(docid string) (bool, string) {
  res, err := lb.req.do("GET", docid, "")
  
  if len(err) > 0 {
    return false, err
  }

  return (res.StatusCode == 200), ""
}

func (lb *Leanback) delete(docid string) (bool, string) {
  res, err := lb.req.do("DELETE", docid, "")
  
  if len(err) > 0 {
    return false, err
  }
  
  return (res.StatusCode == 200), ""
}

func (lb *Leanback) all_docs() ([]Row, string) {
  res, err := lb.req.do("GET", "_all_docs", "")
  
  if len(err) > 0 {
    return nil, "Request error"
  }
  
  doc := toResults(res.Body)

  return doc.Rows, err
}

func (re *Request) do(method string, path string, body string) (*http.Response, string) {
  uri := re.url + "/" + path
  
  req, err := http.NewRequest(method, uri, bytes.NewBufferString(body))
  req.Header.Add("Accept", "application/json")
  
  if len(body) > 0 {
    cl := strconv.Itoa(len(body))
    req.Header.Add("Content-Length", cl)
    req.Header.Add("Content-Type", "application/json")
  }
  
  res, err := http.DefaultClient.Do(req)
    
  if err != nil {
    return nil, "Error Request"
  }
  
  return res, ""
}
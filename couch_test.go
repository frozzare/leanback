package couch

import (
  "fmt"
  "testing"
//  "encoding/json"
//  "bytes"
)

func Connection() (Leanback) {
  //couch := Couch("http://blomman.iriscouch.com")
  couch := Couch("http://192.168.0.23:5984")
  db := couch.use("leanback")
  return db
}

func Get_Document(t *testing.T) {
  db := Connection()
  doc, err := db.get("cae0455faf36ad93a24ddf6b7f0003c1")

  if len(err) > 0 {
    fmt.Println(err)
  } else {
    fmt.Println(fmt.Sprintf("CouchDB (%s) Document _id: %s", db.database, doc["_id"]))
  }
}

func Create_Document(t *testing.T) {
  db := Connection()
  
  //document := struct {
  //  a string
  //}{"This is a test"}
  
  document := "{ \"a\" : \"this is a test via go\" }"
  
  doc, err := db.save(document)
  
  if len(err) > 0 {
    fmt.Println(err)
  } else {
    fmt.Println(fmt.Sprintf("CouchDB (%s) Document _id: %s", db.database, doc["id"]))
  }
}

func Test_All_Docs(t *testing.T) {
  db := Connection() 
  
  docs, _ := db.all_docs()
  
  for _, v := range docs {
    doc, _ := db.get(v.id)
    fmt.Println(doc)
  }
}

func Exsists_Document(t *testing.T) {
  db := Connection()
  
  exists, err := db.exists("29f741acbf6a80d628e0a4e60d0033c00")
  
  if len(err) > 0 {
    fmt.Println(err)
  } else {
    if exists {
      fmt.Println("Document exists")
    } else {
      fmt.Println("Document missing")
    }
  }
}